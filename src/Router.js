import Component from './components/Components.js';
import PizzaList from './pages/PizzaList.js';

export default class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		Router.routes.forEach(element => {
			if (element.path == path) {
				this.titleElement.innerHTML = new Component(
					'h1',
					null,
					element.title
				).render();

				this.contentElement.innerHTML = element.page.render();
			}
		});
	}
}
