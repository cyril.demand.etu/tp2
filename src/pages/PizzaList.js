import PizzaThumbnail from '../components/PizzaThumbnail.js';
import Component from '../components/Components.js';

export default class PizzaList extends Component {
	#pizzas;
	constructor(data) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			data.map(pizza => new PizzaThumbnail(pizza))
		);
		this.#pizzas = data;
	}

	set pizzas(data) {
		this.#pizzas = data;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
