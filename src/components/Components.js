class Component {
	tagName;
	children;
	attribute;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		if (children instanceof Array) {
			this.children = '';
			children.forEach(e => {
				if (e instanceof Component) {
					this.children += e.render();
				} else {
					this.children += e;
				}
			});
		} else {
			this.children = children;
		}
	}
	render() {
		if (this.children instanceof Array) {
			let res = '';
			this.children.forEach(e => {
				if (e instanceof Component) {
					res += e.render();
				} else {
					this.children += e;
				}
			});
			return res;
		}
		if (this.attribute != undefined && this.attribute != '') {
			if (this.children != undefined) {
				return `<${this.tagName} ${this.attribute.name}= "${this.attribute.value}">${this.children}</${this.tagName}>`;
			} else {
				return `<${this.tagName} ${this.attribute.name}= "${this.attribute.value}" />`;
			}
		} else {
			if (this.children != undefined) {
				return `<${this.tagName}>${this.children}</${this.tagName}>`;
			} else {
				return `<${this.tagName}></${this.tagName}>`;
			}
		}
	}
	renderChildren() {
		return `${this.children}`;
	}
}

export default Component;
